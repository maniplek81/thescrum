from django.apps import AppConfig


class ProjectmanagmentConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'projectManagment'
